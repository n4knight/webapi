﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiClass.Model.ErrorModel
{
    public class ErrorModel
    {
        public string Message { get; set; }
        public int StatusCode { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClass.Data.Interfaces;
using WebApiClass.Model.Entities;
using WebApiClass.Service.Interfaces;

namespace WebApiClass.Api.Filters
{
    public class AccountExitsFilter: IAsyncActionFilter
    {
        private readonly IUnitofWork _unitOfWork;
        private readonly IAccountService _accountService;

        public AccountExitsFilter(IUnitofWork _unitOfWork, IAccountService _accountService)
        {
            this._unitOfWork = _unitOfWork;
            this._accountService = _accountService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var id = (Guid)context.ActionArguments["id"];
            var account = _accountService.GetAccountById(id);

            if (account is null)
            {
                context.Result = new NotFoundResult();
                return;
            }

            context.HttpContext.Items.Add("Account", account);
            await next();
        }
    }
}

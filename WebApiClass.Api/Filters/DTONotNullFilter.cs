﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiClass.Api.Filters
{
    public class DTONotNullFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var param = context.ActionArguments.SingleOrDefault(d => d.Value.ToString().Contains("DTO")).Value;
            if (param is null)
            {
                context.Result = new BadRequestResult();
                return;
            }

            await next();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClass.Model.Entities;

namespace WebApiClass.Api.Configurations
{
    public class DatabaseSeeder
    {
        public static void Seed(WebApiContext context)
        {

            context.Database.Migrate();

            if (!context.Customers.Any())
            {
                context.AddRange(new[]
                {
                    new Customer
                    {
                        FullName = "John Doe",
                        Email = "john.D@email.com",
                        Address = "Abakpa",
                        DateOfBirth = DateTime.Now,
                        Phone = "8080"
                    },

                     new Customer
                    {
                        FullName = "Ace Doe",
                        Email = "Ace.D@email.com",
                        Address = "Abakpa",
                        DateOfBirth = DateTime.Now,
                        Phone = "80800"
                    },

                      new Customer
                    {
                        FullName = "Mary Doe",
                        Email = "Mary.D@email.com",
                        Address = "Abakpa",
                        DateOfBirth = DateTime.Now,
                        Phone = "8080000"
                    }

                });

                context.SaveChanges();
            }

            if (!context.Accounts.Any())
            {

                var customer1 =
                    context.Customers.SingleOrDefault(c => c.FullName.Equals("John Doe") && c.Email.Equals("john.D@email.com"));

                var customer2 =
                    context.Customers.SingleOrDefault(c => c.FullName.Equals("Ace Doe") && c.Email.Equals("Ace.D@email.com"));

                var customer3 =
                    context.Customers.SingleOrDefault(c => c.FullName.Equals("Mary Doe") && c.Email.Equals("Mary.D@email.com"));

                context.AddRange(new[]
                {
                        new Account
                        {
                            CustomerId = customer1.Id,
                            Number = "123",
                            Balance = 10000,

                        },
                        new Account
                        {
                            CustomerId = customer2.Id,
                            Number = "1234",
                            Balance = 10000,

                        },
                        new Account
                        {
                            CustomerId = customer3.Id,
                            Number = "12345",
                            Balance = 10000,

                        }

                });

                context.SaveChanges();

            }
        }
    }
}

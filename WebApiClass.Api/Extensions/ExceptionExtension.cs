﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;
using WebApiClass.Model.ErrorModel;

namespace WebApiClass.Api.Extensions
{
    public static class ExceptionExtension
    {
        public static void ConfigureExceptionHandler (IApplicationBuilder app)
        {
            app.UseExceptionHandler(appError =>
           {
               appError.Run(async context =>
              {
                  context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                  context.Response.ContentType = "Application/Json";

                  var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                  if (contextFeature != null)
                  {
                      await context.Response.WriteAsync(new ErrorModel
                      {
                          StatusCode = context.Response.StatusCode,
                          Message = "Internal Server Error"
                      }.ToString());
                  }
              });
           });
        }
    }
}

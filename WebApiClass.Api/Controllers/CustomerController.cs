﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClass.Model.Dtos;
using WebApiClass.Model.Entities;
using WebApiClass.Service.Interfaces;

namespace WebApiClass.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomerController(ICustomerService customerService, IMapper _mapper)
        {
            _customerService = customerService;
            this._mapper = _mapper;
        }

        [HttpGet("")]
        [ProducesResponseType(typeof(Customer),200)]
        public async Task<IActionResult> Customers()
        {
            return Ok(await _customerService.GetCustomersAsync());
        }

        [HttpGet("id")]
        public IActionResult GetCustomer(Guid id)
        {
            var customer = _customerService.GetSingleCustomer(id);

            if (customer is null) return NotFound("no customer found");

            //var customerMappedEntity = _mapper.Map<ViewAccountDto>(customer);
            return Ok(customer);
        }

        [HttpGet("customerAccountDetail/{accountNumber}")]
        public async Task<IActionResult> AccountDetail(string accountNumber)
        {
            return Ok(await _customerService.GetCustomerByAccountNumber(accountNumber));
        }

        [HttpPost]
        public IActionResult CreateCustomer()
        {
            return Ok();
        }

    }
}

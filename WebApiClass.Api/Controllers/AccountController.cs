﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiClass.Api.Filters;
using WebApiClass.Model.Dtos;
using WebApiClass.Model.Entities;
using WebApiClass.Service.Interfaces;

namespace WebApiClass.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IServiceFactory _serviceFactory;
        private readonly IMapper _mapper;

        public AccountController(IAccountService _accountService, IMapper _mapper, IServiceFactory _serviceFactory)
        {
            this._accountService = _accountService;
            this._mapper = _mapper;
            this._serviceFactory = _serviceFactory;
        }

        [HttpGet("")]
        public  Task<IEnumerable<Account>> GetAccount() =>
             _accountService.GetAccounts();

        [HttpGet("id")]
        [ServiceFilter(typeof(AccountExitsFilter))]
        public async Task<IActionResult> GetAccountById(Guid id)
        {

            var account = HttpContext.Items["Account"] as Account;

            var customerService = _serviceFactory.GetServices<ICustomerService>();
            var customer = await customerService.GetSingleCustomer(account.CustomerId);

            var accountEntity = _mapper.Map<ViewAccountDto>(account);
            accountEntity.Name = customer.FullName;

            return Ok(accountEntity);
        }

        [HttpPost("/Deposit")]
        [ServiceFilter(typeof(DTONotNullFilter))]
        public async Task<IActionResult> AccountDeposit(DepositDTO depositDTO)
        {
            var isDeposited = await _accountService.Deposit(depositDTO.AccountNumber, depositDTO.Amount);
            return Ok(isDeposited);
        }

        [HttpPost("/Withdraw")]
        [ServiceFilter(typeof(DTONotNullFilter))]
        public async Task<IActionResult> AccountWithdraw(WithdrawDTO withdrawDTO)
        {
            var amountWithdrawn = await _accountService.Withdraw(withdrawDTO.AccountNumber, withdrawDTO.Amount);
            return Ok(amountWithdrawn);
        }
    }
}
